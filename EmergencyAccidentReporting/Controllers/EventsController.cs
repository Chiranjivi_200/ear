﻿using EmergencyAccidentReporting.Models;
using Microsoft.AspNetCore.Mvc;

namespace EmergencyAccidentReporting.Controllers
{
    public class EventsController : Controller
    {
        DataContext dal = new DataContext();
        public IActionResult Index(int id)
        {
            Events events = dal.eventss.Find(id);
            ViewData["Title"] = events.Title;

            return View(events);

        }
    }
}
