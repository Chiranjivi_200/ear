﻿using EmergencyAccidentReporting.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Security.Cryptography;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using System.Text;

namespace EmergencyAccidentReporting.Controllers
{
    public class LoginController : Controller
    {

        DataContext dal = new DataContext();
        #region Login
        //GET
        public IActionResult Index()
        {
            ViewData["Title"] = "Login";
            return View();
        }
        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public IActionResult Index(UserLogin userlogin)
        {
            ViewData["Title"] = "Login";

            var username_List = dal.userlogins.Where(x => x.username.Equals(userlogin.username)).ToList();
            var user_List = dal.userlogins.Where(x => x.username.Equals(userlogin.username)).Where(y => y.password.Equals(Encryption(userlogin.password))).ToList();


            //removing the fields for modelState validation
            ModelState.Remove("role");






            ModelState.Remove("email");





            



            if (username_List.Count() == 0)
            {
                ModelState.AddModelError("username", "sorry this username donot exist");
            }
            else if (user_List.Count() == 0)
            {
                ModelState.AddModelError("password", "sorry your password donot match");

            }
            if (ModelState.IsValid)
            {

                if (IsAdmin(user_List[0].role))
                {
                    //admin
                    //setting the session for login
                    HttpContext.Session.SetString("logged", "true");
                    HttpContext.Session.SetString("username", userlogin.username);
                    HttpContext.Session.SetString("password", Encryption(userlogin.password));
                    HttpContext.Session.SetString("role", user_List[0].role);
                    HttpContext.Session.SetString("email", user_List[0].email);

                    return RedirectToAction("Index", "Admin");

                }
                else if (!IsAdmin(user_List[0].role))
                {
                    HttpContext.Session.SetString("logged", "true");
                    HttpContext.Session.SetString("username", userlogin.username);
                    HttpContext.Session.SetString("password", Encryption(userlogin.password));
                    HttpContext.Session.SetString("role", user_List[0].role);
                    HttpContext.Session.SetString("email", user_List[0].email);
                    return RedirectToAction("Index", "Landing");

                }
            }

            return View(userlogin);



        }

        //checking role for admin or user.
        public bool IsAdmin(string role)
        {
            bool _IsAdmin = true;

            if (role != "admin")
            {

                _IsAdmin = false;
            }

            return _IsAdmin;
        }
        #endregion

        #region SignUp

        //get
        public IActionResult Signup()
        {
            ViewData["Title"] = "Sign Up";

            return View();
        }

        //post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Signup(UserLogin userlogin)
        {
            ViewData["Title"] = "Sign Up";
            var users = dal.userlogins.ToList();
            //removing role to check for ModelState since it is defined explicitly here.

            //checking if the user already exists
            if (dal.userlogins.Where(x => x.username.Equals(userlogin.username)).Count() >= 1)
            {
                ModelState.AddModelError("username", "this username already taken");
            }
            /* if (dal.userlogins.Where(x => x.email.Equals(userlogin.email)).Count() >= 1)
             {
                 ModelState.AddModelError("email", "email already registered");
             }*/

            if (CheckUser(users, userlogin.email))
            {
                ModelState.AddModelError("email", "already registered with this email");
            }

           // bool istrue = CheckUser(users, userlogin.email);

            ModelState.Remove("role");
            userlogin.role = "user";
            if (ModelState.IsValid)
            {
                userlogin.password = Encryption(userlogin.password);
                dal.userlogins.Add(userlogin);
                dal.SaveChanges();

                //success message
                return View(userlogin);
            }
            //if ModelState is not valid than the typed data will remain same with some error message
            return View(userlogin);
        }

        //md5 encryption
        public string Encryption(String password)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] encrypt;
            UTF8Encoding encode = new UTF8Encoding();
            //encrypt the given password string into Encrypted data
            encrypt = md5.ComputeHash(encode.GetBytes(password));
            StringBuilder encryptdata = new StringBuilder();
            //create a new string by using the encrypted data
            for (int i = 0; i < encrypt.Length; i++)
            {
                encryptdata.Append(encrypt[i].ToString());
            }
            return encryptdata.ToString();

        }

        #endregion

        #region Logout

        //yo action kai call vako xaina.
        public IActionResult Logout()
        {
            HttpContext.Session.SetString("User", "null");
            HttpContext.Session.SetString("logged", "null");
            return RedirectToAction("Index", "Landing");
        }

        #endregion


        public IActionResult Change()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Change(ChangePassword change)
        {

            //custom model error.

            if (change.newpassword != change.confirmpassword)
            {
                ModelState.AddModelError("newpassword", "password donot match");
                ModelState.AddModelError("confirmpassword", "password donot match");
            }
            String password = HttpContext.Session.GetString("password");
            if (change.oldpassword != password)
            {
                ModelState.AddModelError("oldpassword", "sorry your current password donot match");
            }

            if (ModelState.IsValid)
            {
                String username = HttpContext.Session.GetString("username");
                UserLogin thisuser = dal.userlogins.Where(x => x.username.Equals(username)).SingleOrDefault();
                thisuser.password = change.confirmpassword;
                dal.userlogins.Update(thisuser);
                dal.SaveChanges();
                return RedirectToAction("Logout", "Login");
            }


            return View(change);
        }


        
        //binary search algorithem for user exist search
        private static bool CheckUser(List<UserLogin> userrlogin, string email)
        {
            int left = 0;
            int right = userrlogin.Count-1;
            bool check = true;
            while (left < right)
            {
                int mid = left + ((right - left) / 2);
                if (userrlogin[mid].email.Equals(email))
                {
                    return true;
                }
                else
                {
                    for (int i = left; i < mid; i++)
                    {
                        if (userrlogin[i].email.Equals(email))
                        {
                            check = true;
                            right = mid - 1;
                        }
                        else
                        {
                            check = false;
                        }
                    }
                    for (int j = mid; j < right; j++)
                    {
                        if (userrlogin[j].email.Equals(email))
                        {
                            check = true;
                            left = mid + 1;
                        }
                        else
                        {
                            check = false;
                        }
                    }
                }
                if (check == false)
                {
                    return false;
                }
            }
            return false;
        }
       
    }
}
