﻿using EmergencyAccidentReporting.Models;
using Microsoft.AspNetCore.Mvc;

namespace EmergencyAccidentReporting.Controllers
{
    public class NewsController : Controller
    {

        DataContext dal = new DataContext();




        public IActionResult Index(int id)
        {





            News news = dal.newss.Find(id);
            ViewBag.relatedNews = dal.newss.Where(x => x.Category.Equals(news.Category));

            Recommendation recommendation = new Recommendation();
            if (HttpContext.Session.GetString("logged") == "true")
            {
                recommendation.username = HttpContext.Session.GetString("username");
                recommendation.category = news.Category;

            }
            dal.recommendations.Add(recommendation);

            dal.SaveChanges();
            ViewData["Title"] = news.Title;

            return View(news);
        }




    }
}
