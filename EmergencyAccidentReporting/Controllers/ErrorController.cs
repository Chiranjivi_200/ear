﻿using Microsoft.AspNetCore.Mvc;

namespace EmergencyAccidentReporting.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult SessionOut()
        {
            ViewData["Title"] = "Session Error";
            ViewBag.SessionOut = "Sesion Expired";
            return View();
        }
    }
}
