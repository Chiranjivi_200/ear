﻿using EmergencyAccidentReporting.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Session;

namespace EmergencyAccidentReporting.Controllers
{
    public class LandingController : Controller
    {
        DataContext dal = new DataContext();
        public IActionResult Index()
        {
            ViewData["TiTle"] = "Dashboard";
            return View();

        }

        //navigation to certain sectin
        public IActionResult Navigate_contact()
        {
            ViewData["Title"] = "Contact";
            return RedirectToAction("Index", "Landing", "contact");
        }

        public IActionResult Navigate_about()
        {
            ViewData["Title"] = "About";

            return RedirectToAction("Index", "Landing", "about");
        }
        public IActionResult News()
        {
            ViewData["Title"] = "News";

            var recommendation = dal.recommendations.ToList();
            var noofdata = recommendation.Where(x => x.username.Equals(HttpContext.Session.GetString("username"))).Count();
            IEnumerable<News> news = dal.newss;
            if (noofdata >= 1)
            {
                HttpContext.Session.SetString("_isrecommend", "true");
                IEnumerable<News> reconews = news.Where(x => recommendation.Any(y => y.category.Equals(x.Category)));
                ViewBag.recommended_data = reconews;
            }
            
            return View(news);


        }

        [HttpPost]
        public IActionResult News(string query)
        {

            var recommendation = dal.recommendations.ToList();
            var noofdata = recommendation.Where(x => x.username.Equals(HttpContext.Session.GetString("username"))).Count();
            IEnumerable<News> news = dal.newss.Where(s => s.NewsDetail.Contains(query) || s.Title.Contains(query));
            if (noofdata >= 1)
            {
                HttpContext.Session.SetString("_isrecommend", "true");
                IEnumerable<News> reconews = news.Where(x => recommendation.Any(y => y.category.Equals(x.Category)));
                ViewBag.recommended_data = reconews;
            }

            return View(news);
        }
        public IActionResult Navigate_portfolio()
        {
            return RedirectToAction("Index", "Landing", "portfolio");
        }

        public IActionResult Event()
        {
            ViewData["Title"] = "Events";
            IEnumerable<Events> events = dal.eventss;
            return View(events);

        }
    }
}
