﻿using EmergencyAccidentReporting.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace EmergencyAccidentReporting.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

	//getting IpAddress Test-- comment test for new branch
        public void GetIpAddress()
        {
            int x = 10;
            int y = 20;
            //new comment
        }
        public void Pawan()
        {

        }
    }
}