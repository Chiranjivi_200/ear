﻿using EmergencyAccidentReporting.Models;

using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace EmergencyAccidentReporting.Controllers
{
    [Route("api/demo")]
    public class DemoController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        public DemoController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }
        /// <summary>
        /// This Demo Controller is used to create the projects' backend operation so it can be later used by the
        /// </summary>
        //data context instance
        DataContext dal = new DataContext();

        /// <summary>
        /// Ajax tutorial
        /// </summary>
        /// <returns></returns>
        #region Ajax

      /*  [Route("")]
        [Route("index")]
        [Route("~/")]*/
        public async Task<IActionResult> Index()
        {
            return View();
        }
        #endregion

        #region API returns string only

        
        [Route("demo1")]
        public ContentResult Demo1()
        {
            return Content("hello", "text/plain");

        }

        #endregion



        #region Lost Vehicle Region
        //CRUD lost vehicle.
        //INSERT
        [Consumes("application/json")]
        [Produces("application/json")]
        [HttpPost("register")]
        public async Task<IActionResult> VehicleRegister([FromBody] Vehicle vehicle)
        {
            try
            {
                vehicle.status = "pending";
                dal.Add(vehicle);
                dal.SaveChanges();
                return Ok("inserted in database");//dialogue box to ensure the given data is correct.

            }
            catch
            {

                return BadRequest();
            }
        }


        //SELECT
        [Produces("text/plain")]
        [HttpGet("vehicledetails")]
        public async Task<IActionResult> VehicleDetails()
        {

            try
            {
                List<Vehicle> vehicles = dal.vehicles.ToList();
                var user = JsonConvert.SerializeObject(vehicles);
                return Ok(user);
            }
            catch
            {

                return BadRequest();
            }
        }

        //DELETE

        [Consumes("application/json")]
        [Produces("application/json")]
        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> DeleteVehicleReport(int id)
        {
            try
            {
                Vehicle result = dal.vehicles.Find(id);
                dal.vehicles.Remove(result);
                dal.SaveChanges();
                return Ok(result);
            }
            catch
            {

                return BadRequest();
            }
        }



        /// <summary>
        /// in order to update we must first navigate to the update page 
        /// 1. so we create NavigatetoUpdate(id) action
        /// 2. and create the UpdateVehicleReport
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        /// 

        [HttpGet("vehicle/{id}")]
        [Produces("appliction/json")]
        public async Task<IActionResult> UpdateVehiclePage(int id)
        {
            Vehicle vehicle = dal.vehicles.Find(id);
            return Ok(vehicle);
        }

        //UPDATE

        [Consumes("application/json")]
        [Produces("application/json")]
        [HttpPost("update")]
        public async Task<IActionResult> UpdateVehicleReport([FromBody] Vehicle vehicle)
        {
            try
            {
                var result = vehicle;
                dal.vehicles.Update(vehicle);
                dal.SaveChanges();

                return Ok(result);
            }
            catch
            {

                return BadRequest();
            }
        }


        /// <summary>
        /// image uploading file POST
        /// </summary>
        /// <returns></returns>
        /// 






        //demo api
        [Produces("text/plain")]
        [HttpGet("demo11")]
        public async Task<IActionResult> Demo11()
        {

            try
            {
                string result = "demo 2";
                return Ok(result);
            }
            catch
            {
                return BadRequest();

            }

        }
        #endregion


        #region GPS API

        //getting the coordinate of the device
        //understanding the google api first.





        #endregion



        #region Multiple Image Insertion From API

        //Image upload function

        /* public async Task<string> UploadImage(string folderpath, IFormFile file)
         {
             folderpath += Guid.NewGuid().ToString() + "_" + file.FileName;
             string serverFolder = Path.Combine(_webHostEnvironment.WebRootPath, folderpath);
             await file.CopyToAsync(new FileStream(serverFolder, FileMode.Create));
             return "/" + folderpath;
         }*/

        /// <summary>
        /// //errrrrrrrrrrrrrrooooooooooooorrrrrrrrrrrrrrrrrrrrrrrrrrr.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("image")]
        //[Consumes("application/json")]
        [Consumes("multipart/form-data")]
        [Produces("multipart/form-data")]
        public async Task<IActionResult> Image([FromBody] UploadFile obj)
        {
            try
            {
                string folderpath = "wwwroot/image";
                Product product = new Product();
                product.Name = obj.Name;
                product.ImageUrl = "customUrl";
                //product.ImageUrl = "hello";//await UploadImage(folderpath, file); //use the function here from Doctor Patient Model

                dal.products.Add(product);
                dal.SaveChanges();

                return Ok("200");
            }
            catch
            {
                return BadRequest();
            }
        }
        #endregion


        #region LoginCheck


        [HttpPost("login")]
        [Produces("text/plain")]
        [Consumes("text/plain")]
        public async Task<IActionResult> Login(string username, string password)
        {

            try
            {

                var user_List = dal.userlogins.Where(x => x.username.Equals(username)).Where(y => y.password.Equals(password)).ToList();

                if (user_List.Count() == 1 && user_List[0].password.Equals(password) && user_List[0].role.Equals("admin"))
                {
                    return Ok("admin logged in");
                }
                else if (user_List.Count() == 1 && user_List[0].password.Equals(password) && user_List[0].role.Equals("user"))
                {

                    return Ok("user logged in");
                }

                else
                {
                    // TempData["error"] = "invalid credentials";
                    // return RedirectToAction("Index", "Login");
                    return Ok("notlogged in");
                }
            }
            catch
            {

                return BadRequest();

            }
        }
        #endregion


        #region SignUP


        #endregion




    }

}
