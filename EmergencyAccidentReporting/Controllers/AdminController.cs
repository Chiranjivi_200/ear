﻿using EmergencyAccidentReporting.Models;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using Newtonsoft.Json;


namespace EmergencyAccidentReporting.Controllers
{
    public class AdminController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        public AdminController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }
        public async Task<string> UploadImage(string folderpath, IFormFile file)
        {
            folderpath += Guid.NewGuid().ToString() + "_" + file.FileName;
            string serverFolder = Path.Combine(_webHostEnvironment.WebRootPath, folderpath);
            await file.CopyToAsync(new FileStream(serverFolder, FileMode.Create));
            return "/" + folderpath;
        }

        public async Task<string> UpdateImage(IFormFile file)
        {
            return Guid.NewGuid().ToString() + "_" + file.FileName;
        }

        DataContext dal = new DataContext();
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("logged") == "true") ;
            {
                ViewBag.totalaccident = dal.accidents.ToList().Count;
                ViewBag.totallostvehicle = dal.vehicles.ToList().Count;
                ViewBag.totaluser = dal.userlogins.ToList().Count;

                return View();
            }
            return RedirectToAction("SessionOut", "Error");
        }




        #region Lost Vehicle

        //select
        public async Task<IActionResult> LostReports()
        {
            try
            {
                IEnumerable<Vehicle> vehicleObj = dal.vehicles;
                return View(vehicleObj);
            }
            catch
            {

                return BadRequest();
            }
        }

        //go to Update page with data

        [HttpGet]
        public IActionResult UpdateVehiclePage(int id)
        {
            Vehicle vehicle = dal.vehicles.Find(id);
            ViewBag.status = new List<string>() { "found", "pending" };
            return View(vehicle);
        }

        //changing status to found i.e. to and forth
        [HttpGet]
        public IActionResult FoundVehicle(int id)
        {

            Vehicle vehicle = dal.vehicles.Find(id);
            vehicle.status = "found";
            dal.vehicles.Update(vehicle);
            dal.SaveChanges();

            /*//Mailing
            MimeMessage message = new MimeMessage();
            SmtpClient client = new SmtpClient();


            message.From.Add(new MailboxAddress("EAR", "doctorpatientappointment.portal@gmail.com"));
            message.To.Add(MailboxAddress.Parse(vehicle.Email));
            message.Subject = "Vehicle Found Alert !!";
            message.Body = new TextPart("plain")
            {
                Text = "Mr/Mrs" + vehicle.OwnerName + "Your Vehicle is found so please visit the office for further processing",
            };

            //connect
            client.Connect("smtp.gmail.com", 465, true);

            client.AuthenticationMechanisms.Remove("XOAUTH2");
            //email-id , password
            client.Authenticate("doctorpatientappointment.portal@gmail.com", "99884444669944995588");
            //send mail
            client.Send(message);*/
            return RedirectToAction("LostReports", "Admin");
        }

        //updateaction
        [HttpPost]
        public IActionResult UpdateVehicleAction(Vehicle vehicle)
        {
            //vehicle.status = "found";
            dal.vehicles.Update(vehicle);
            dal.SaveChanges();
            return RedirectToAction("LostReports", "Admin");
        }
        #endregion






        // accident report
        #region Accident

        //select
        public async Task<IActionResult> AccidentReports()
        {
            try
            {
                ViewData["Title"] = "Accident Report";
                IEnumerable<Accident> accident = dal.accidents;
                return View(accident);
            }
            catch
            {
                return BadRequest();
            }
        }

        //go to Update page with data

        [HttpGet]
        public IActionResult ViewAccidentPage(int id)
        {
            Accident accident = dal.accidents.Find(id);
            return View(accident);
        }

        //updateaction
        [HttpPost]
        public IActionResult UpdateAccidentAction(Accident accident)
        {
            
            dal.accidents.Update(accident);
            dal.SaveChanges();
            return RedirectToAction("AccidentReports", "Admin");
        }

        #endregion






        public IActionResult ViewNews()
        {
            IEnumerable<News> news = dal.newss;
            return View(news);
        }

        public IActionResult UpdateNews(int id)
        {
            var data = dal.newss.Find(id);
            return View(data);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateNews(News news, IFormFile file)
        {
            string folderpath = "newsimage/";
            news.ImgUrl = await UploadImage(folderpath, file);
            dal.newss.Update(news);
            dal.SaveChanges();
            return RedirectToAction("ViewNews", "Admin");
        }
        //News
        public IActionResult AddNews()
        {
            ViewData["Title"] = "Add News";
            ViewBag.cat = new List<string>() { "accident", "Vehicle", "sports" };
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddNews(NewsModel newsmodel)
        {
            ViewData["Title"] = "Add News";
            string folderpath = "newsimage/";

            ModelState.Remove("Date");
            ModelState.Remove("Category");
            if (ModelState.IsValid)
            {
                News news = new News()
                {

                    ImgUrl = await UploadImage(folderpath, newsmodel.ImgUrl),
                    Date = DateTime.Now.ToString("MM/dd/yyyy"),
                    NewsDetail = newsmodel.NewsDetail,
                    Title = newsmodel.Title,
                    Category = newsmodel.Category,
                };


                dal.newss.Add(news);
                dal.SaveChanges();
                ModelState.Clear();
                return RedirectToAction("AddNews", "Admin");
            }

            return View(newsmodel);
        }

        //event ----------------------------------------------------
        public IActionResult ViewEvents()
        {
            IEnumerable<Events> events = dal.eventss;

            return View(events);

        }
        public IActionResult UpdateEvent(int id)
        {
            var data = dal.eventss.Find(id);
            return View(data);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateEvent(Events events, IFormFile file)
        {
            string folderpath = "eventsimage/";
            events.ImgUrl = await UploadImage(folderpath, file);
            dal.eventss.Update(events);
            dal.SaveChanges();
            return RedirectToAction("ViewEvents", "Admin");
        }
        public IActionResult AddEvents()
        {
            ViewData["Title"] = "Add Events";
            return View();
        }
        //AddEvents
        [HttpPost]
        public async Task<IActionResult> AddEvents(EventsModel eventsmodel)
        {
            ViewData["Title"] = "Add Events";
            string folderpath = "eventsimage/";

            ModelState.Remove("Date");
            if (ModelState.IsValid)
            {
                Events events = new Events()
                {

                    ImgUrl = await UploadImage(folderpath, eventsmodel.ImgUrl),
                    Date = DateTime.Now.ToString("MM/dd/yyyy"),
                    EventsDetail = eventsmodel.EventsDetail,
                    Title = eventsmodel.Title,
                };


                dal.eventss.Add(events);
                dal.SaveChanges();
                ModelState.Clear();
                return View();
            }

            return View(eventsmodel);
        }


        public ActionResult topawan()
        {
            return Redirect("https://www.pawanbastola.com.np");
        }

        //route

        public ActionResult Route(string usercoordinate, string mycoordinate)
        {
            string[] splittocoordinate = usercoordinate.Split(",");
            string[] splitfromcoordinate = mycoordinate.Split(",");



            string url = "https://www.google.com/maps/dir/";
            string first_lat = splitfromcoordinate[0];
            string first_lon = splitfromcoordinate[1];

            string second_lat = splittocoordinate[0];
            string second_lon = splittocoordinate[1];


            string concatedurl = url + first_lat + ",+" + first_lon + "/" + second_lat + ",+" + second_lon;
            /*
            26.640184,+87.925248            ---- - string 1
            / 26.658507,+87.861052              -- - string 2
            /@26.6495559,87.8756352,14z--string 3"
            */

            return Redirect(concatedurl);
        }




        //changing status to found i.e. to and forth




       

    }
}
