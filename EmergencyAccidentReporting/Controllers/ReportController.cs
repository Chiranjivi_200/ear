﻿using EmergencyAccidentReporting.Models;
using Microsoft.AspNetCore.Mvc;

namespace EmergencyAccidentReporting.Controllers
{
    public class ReportController : Controller
    {

        private readonly IWebHostEnvironment _webHostEnvironment;
        public ReportController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        DataContext dal = new DataContext();



        #region Vehicle

        //get
        public IActionResult LostVehicle()
        {
            ViewData["Title"] = "Lost Vehicle";

            ViewBag.type = new List<string>() { "Motorcycle", "Scooter", "Moped", "Car", "jeep", "Delivery Van", "Tempo", "Auto Rickshaw", "Tractor", "Lorrey", "Bus" };

            return View();

        }
        //post

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LostVehicle(Vehicle vehicle)
        {
            ViewData["Title"] = "Lost Vehicle";


            vehicle.status = "pending";
            vehicle.date = DateTime.Now.ToString("MM/dd/yyyy");
            ModelState.Remove("status");//removing status from validation check. 
            ModelState.Remove("date");

            if (ModelState.IsValid)
            {
                dal.Add(vehicle);
                dal.SaveChanges();
                return RedirectToAction("Index", "Landing");//dialogue box to ensure the given data is correct.
            }
            ViewBag.type = new List<string>() { "Motorcycle", "Scooter", "Moped", "Car", "jeep", "Delivery Van", "Tempo", "Auto Rickshaw", "Tractor", "Lorrey", "Bus" };

            return View(vehicle);
        }
        #endregion

        #region Non View Functions
        /// <summary>
        /// 1. UploadImage
        /// </summary>
        /// <returns></returns>
        public async Task<string> UploadImage(string folderpath, IFormFile file)
        {
            folderpath += Guid.NewGuid().ToString() + "_" + file.FileName;
            string serverFolder = Path.Combine(_webHostEnvironment.WebRootPath, folderpath);
            await file.CopyToAsync(new FileStream(serverFolder, FileMode.Create));
            return "/" + folderpath;
        }
        #endregion

        //multiple image insertion.




        #region Accident

        //get
        public IActionResult Accident()
        {
            ViewData["Title"] = "Accident";

            return View();
        }

        //post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Accident(AccidentViewModel AccidentReport)
        {
            ViewData["Title"] = "Accident";

            string folderpath = "image/";

            ModelState.Remove("Date");
            ModelState.Remove("mycoordinate");

            if (ModelState.IsValid)
            {

                Accident accident = new Accident()
                {
                    Number = AccidentReport.Number,
                    Image_Urls = await UploadImage(folderpath, AccidentReport.file),
                    Contact = AccidentReport.Contact,
                    Location = AccidentReport.Location,
                    ReportersName = AccidentReport.ReportersName,
                    Geolocation = AccidentReport.Geolocation,
                    Date = DateTime.Now.ToString("MM/dd/yyyy"),
                };
                //product.ImageUrl = "hello";//await UploadImage(folderpath, file); //use the function here from Doctor Patient Model

                dal.accidents.Add(accident);
                dal.SaveChanges();
                return RedirectToAction("Index", "Landing");
            }
            return View(AccidentReport);
        }
        #endregion





        ///test
        ///


    }
}
