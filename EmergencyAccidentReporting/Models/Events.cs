﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmergencyAccidentReporting.Models
{

    [Table("events")]
    public class Events
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Date { get; set; }
        public string EventsDetail { get; set; } //we are gonna use ck-editor for description.
        public string ImgUrl { get; set; }
    }

    public class EventsModel
    {

        public int Id { get; set; }
        public string Title { get; set; }
        public string Date { get; set; }
        public string EventsDetail { get; set; }
        public IFormFile ImgUrl { get; set; }
    }
}
