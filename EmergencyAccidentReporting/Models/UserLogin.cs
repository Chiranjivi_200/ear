﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmergencyAccidentReporting.Models
{
    [Table("userlogin")]
    public class UserLogin
    {
        [Key]
        public int uid { get; set; }
        [Required]
        [DisplayName("Username")]
        public string username { get; set; }
        [Required]
        [DisplayName("Password")]
        public string password { get; set; }
        public string role { get; set; }

        [Required]
        [EmailAddress]
        public string email { get; set; }

        //creating the constructor

    }

    public class ChangePassword
    {
        [Required]
        public string oldpassword { get; set; }
        [Required]
        public string newpassword { get; set; }
        [Required]
        public string confirmpassword { get; set; }
    }


}
