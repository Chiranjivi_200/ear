﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EmergencyAccidentReporting.Models
{

    [Table("recomendation")]
    public class Recommendation
    {
        public int id { get; set; }
        public string username { get; set; }
        public string category { get; set; }  
    }
}
