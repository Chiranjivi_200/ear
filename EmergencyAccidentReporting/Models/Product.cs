﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmergencyAccidentReporting.Models
{
    
        [Table("product")]
        public class Product
        {
            [Key]
            public int Id { get; set; }
            public string Name { get; set; }
            public string ImageUrl { get; set; }
        }
    
}
