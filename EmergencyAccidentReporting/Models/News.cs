﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmergencyAccidentReporting.Models
{
    [Table("news")]
    public class News
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }

        public string Date { get; set; }
        public string NewsDetail { get; set; } //we are gonna use ck-editor for description.
        public string ImgUrl { get; set; }
    }

    public class NewsModel
    {

        public int Id { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public string Date { get; set; }
        public string NewsDetail { get; set; }
        public IFormFile ImgUrl { get; set; }
    }
}
