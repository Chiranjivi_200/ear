﻿
using Microsoft.EntityFrameworkCore;

namespace EmergencyAccidentReporting.Models
{
    public class DataContext : DbContext
    {
       
protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            var configuration = builder.Build();
            optionsBuilder.UseMySql(configuration
                ["ConnectionStrings:DefaultConnection"], new MySqlServerVersion(new Version(8, 0, 11)));

        }
        public DbSet<UserLogin> userlogins { get; set; }
        public DbSet<Vehicle> vehicles { get; set; }
        public DbSet<News> newss { get; set; }
        public DbSet<Events> eventss { get; set; }
        public DbSet<Accident> accidents { get; set; }
        public DbSet<Recommendation> recommendations{ get; set; }

        ///testing data models
        ///
        public DbSet<Product> products { get; set; }



    }
}
