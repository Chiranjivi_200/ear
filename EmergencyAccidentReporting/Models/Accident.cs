﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmergencyAccidentReporting.Models
{
    [Table("accident")]
    public class Accident
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Number { get; set; }//vehicle number
        public string Location { get; set; }
        public string Contact { get; set; }
        public string Image_Urls { get; set; }
        public string ReportersName { get; set; }
        public string Geolocation { get; set; }
        public string Date { get; set; }
    }

    public class AccidentViewModel
    {

        [Required]
        [DisplayName("Vehicle Number")]
        public string Number { get; set; }//vehicle number
        [Required]

        //code to give the location cordinates.
        public string Location { get; set; }


        [Required]
        [MaxLength(10, ErrorMessage = "phone number should have 10 numbers")]
        [MinLength(10, ErrorMessage = "phone number should have 10 numbers")]
        public string Contact { get; set; }

        [Required(ErrorMessage = "please insert the image of accidental area")]
        [DisplayName("Photo")]
        public IFormFile file { get; set; }

        [Required]
        [DisplayName("Reporters Name")]
        public string ReportersName { get; set; }

        public string Geolocation { get; set; }

        public string? mycoordinate { get; set; }
        public string Date { get; set; }
    }
}
