﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmergencyAccidentReporting.Models
{
    [Table("vehicle")]
    public class Vehicle
    {
        [Key]
        public int Id { get; set; }//1

        [Required]
        [DisplayName("Owner Name")]
        public string OwnerName { get; set; }//2

        [Required]
        [DisplayName("Vehicle Number")]
        public string Number { get; set; }//3

        [Required]
        [DataType(DataType.PhoneNumber)]
        [MinLength(10,ErrorMessage ="Phone number should contain 10 numbers")]
        [MaxLength(10, ErrorMessage ="Maximum length is 10")]
        [DisplayName("Contact")]
        public string Contact { get; set; }//4

        [Required]
        public string Address { get; set; }//5
        [Required]
        [EmailAddress(ErrorMessage ="this email syntax is not valid")]
        public string Email { get; set; }//6
        [Required]
        public string Type { get; set; }//7

        [Required]
        [DisplayName("Vehicle Model")]
        public string Model { get; set; }//8

        [Required]
        [DisplayName("Vehicle Color")]

        public string Color { get; set; }//9
        [Required]

        public string Description { get; set; } //implement the ck-editor in the description. 10

        [Required]
        public string status { get; set; }//pending, found

        public string date { get; set; }


    }
}
