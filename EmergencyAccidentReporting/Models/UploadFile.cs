﻿namespace EmergencyAccidentReporting.Models
{
    public class UploadFile
    {
        public int Id { get; set; }
        public string Files { get; set; }
        public string Name { get; set; }
    }
}
